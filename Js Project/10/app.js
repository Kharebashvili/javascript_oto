// 2.	მაუსის მოძრაობა მოედანზე. ააგეთ მოედანი, კურსორის მოედაზე მოძრა¬ობისას, კურსორს გააყოლეთ პატარა წითელი კვადრატი. კურსორის მოედნის დატოვებისას კვადრატი არ უნდა გამოჩნდეს.


const mainSquare = document.getElementById('mainSquare');
const smallSquare = document.getElementById('smallSquare');

mainSquare.addEventListener('mousemove', (event) => {
  const mainSquareRect = mainSquare.getBoundingClientRect();
  const x = event.clientX - mainSquareRect.left;
  const y = event.clientY - mainSquareRect.top;

  // პატარა კვადრატი რჩება მთავარი მოედნის საზღვრებში
  const maxX = mainSquareRect.width - smallSquare.clientWidth;
  const maxY = mainSquareRect.height - smallSquare.clientHeight;

  const clampedX = Math.max(0, Math.min(x, maxX));
  const clampedY = Math.max(0, Math.min(y, maxY));

  //  პატარა კვადრატის პოზიცია
  smallSquare.style.left = `${clampedX}px`;
  smallSquare.style.top = `${clampedY}px`;
});

mainSquare.addEventListener('mouseleave', () => {
  // დაიმალება პატარა კვადრატი, როდესაც კურსორი ტოვებს მოედანს
  smallSquare.style.display = 'none';
});

mainSquare.addEventListener('mouseenter', () => {
  // აჩვენებს პატარა კვადრატს, როცა კურსორი მოედანში შედის
  smallSquare.style.display = 'block';
});


// 6.	ააგეთ სკალა, და შექმენით სკალაზე დატანილი ნიშნულის მოძრაობის იმიტაცია

const scale = document.getElementById('scale');
const mark = document.getElementById('mark');

let isDragging = false;

mark.addEventListener('mousedown', (event) => {
  isDragging = true;
  handleMouseMove(event);
});

document.addEventListener('mouseup', () => {
  isDragging = false;
});

document.addEventListener('mousemove', (event) => {
  if (isDragging) {
    handleMouseMove(event);
  }
});

function handleMouseMove(event) {
  const scaleRect = scale.getBoundingClientRect();
  const mouseX = event.clientX - scaleRect.left;

  // ნიშანი რჩება მაშტაბის ფარგლებში
  const minX = 0;
  const maxX = scaleRect.width - mark.clientWidth;

  const clampedX = Math.max(minX, Math.min(mouseX, maxX));

  // განაახლეთ ნიშნის პოზიცია
  mark.style.left = `${clampedX}px`;
}


// 1.	ააგეთ მოედანი, მოედანზე გარკვეული პერიოდულობით სხვადასხვა ადგილ¬ზე(შემთხვევითად) გამოიტანეთ პატარა მწვანე კვადრატი.

const mSquare = document.getElementById('mSquare');

function placeRandomGreenSquare() {
  // Create a green square element
  const greenSquare = document.createElement('div');
  greenSquare.classList.add('greenSquare');

  // Randomly position the green square within the main square
  const maxX = mainSquare.clientWidth - greenSquare.clientWidth;
  const maxY = mainSquare.clientHeight - greenSquare.clientHeight;

  const randomX = Math.floor(Math.random() * maxX);
  const randomY = Math.floor(Math.random() * maxY);

  greenSquare.style.left = `${randomX}px`;
  greenSquare.style.top = `${randomY}px`;

  mSquare.appendChild(greenSquare);

  setTimeout(() => {
    mSquare.removeChild(greenSquare);
  }, 1000);
}

setInterval(placeRandomGreenSquare, 3000);


// 3.	დაფარეთ 500x600-ზე მოედანი  მწვანე, ლურჯი, წითელი ან ყვითე-ლი(შემთხვევითი ფერის) კვადრატებით.

const randomColorSquare = document.getElementById('randomColorSquare');

function getRandomColor() {
  const colors = ['green', 'blue', 'red', 'yellow'];
  const randomIndex = Math.floor(Math.random() * colors.length);
  return colors[randomIndex];
}

function fillSquareWithRandomColors() {
  const squareWidth = randomColorSquare.clientWidth;
  const squareHeight = randomColorSquare.clientHeight;

  const rows = squareHeight / 20;
  const columns = squareWidth / 20;

  for (let i = 0; i < rows; i++) {
    for (let j = 0; j < columns; j++) {
      const colorSquare = document.createElement('div');
      colorSquare.classList.add('colorSquare');

      const randomColor = getRandomColor();
      colorSquare.style.backgroundColor = randomColor;

      const positionX = j * 20;
      const positionY = i * 20;

      colorSquare.style.left = `${positionX}px`;
      colorSquare.style.top = `${positionY}px`;

      randomColorSquare.appendChild(colorSquare);
    }
  }
}

fillSquareWithRandomColors();


