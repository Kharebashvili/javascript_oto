// 5.	შექმენით ელექტორნული საათი:
function updateClock() {
    const clockElement = document.getElementById('clock');
    const currentTime = new Date();

    const hours = currentTime.getHours();
    const minutes = currentTime.getMinutes();
    const seconds = currentTime.getSeconds();

    const formattedHours = formatTimeComponent(hours);
    const formattedMinutes = formatTimeComponent(minutes);
    const formattedSeconds = formatTimeComponent(seconds);

    const formattedTime = `${formattedHours}:${formattedMinutes}:${formattedSeconds}`;
    clockElement.textContent = formattedTime;
  }
 // ეს ნაწილი იმის უზრუნველსაყოფადაა,რომ როცა საათი აჩვენებს მაგ. 8 საათს არ დაწეროს 8, არამედ იყოს 08
  function formatTimeComponent(component) {
    return component < 10 ? '0' + component : component;
  }

  updateClock();
  setInterval(updateClock, 1000);

//   1.	დიალოგური ფანჯარა alert 

document.addEventListener('DOMContentLoaded', function () {
    alert('Click "OK" to display the message.');

    const welcomeMessage = '/Welcome to my site!!!';
    const h1Element = document.createElement('h1');
    h1Element.textContent = welcomeMessage;
    document.body.appendChild(h1Element);
  });