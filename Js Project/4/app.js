// 1.	ააგეთ ქვემოთ მოცემული ფორმა, რიცხვზე დაჭერის შედეგად ტექსტურ ველში გამოიტანეთ კვირის შესაბამისი დღე


const daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const frameElement = document.getElementById('frame');

function displayDayOfWeek(index) {
  const day = daysOfWeek[index];
  frameElement.textContent = day;
}

for (let i = 0; i < daysOfWeek.length; i++) {
  const button = document.createElement('button');
  button.textContent = daysOfWeek[i];
  button.addEventListener('click', () => displayDayOfWeek(i));
  document.body.appendChild(button);
}


