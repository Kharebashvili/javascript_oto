const displayElement = document.getElementById('display');
  let currentInput = '';
  let currentOperation = '';
  let storedValue = '';

  function appendToDisplay(value) {
    currentInput += value;
    displayElement.value = currentInput;
  }

  function performOperation(operation) {
    if (currentOperation !== '') {
      calculateResult();
    }
    storedValue = currentInput;
    currentInput = '';
    currentOperation = operation;
  }

  function calculateResult() {
    let result;
    const num1 = parseFloat(storedValue);
    const num2 = parseFloat(currentInput);

    switch (currentOperation) {
      case '+':
        result = num1 + num2;
        break;
      case '-':
        result = num1 - num2;
        break;
      case '*':
        result = num1 * num2;
        break;
      case '/':
        result = num1 / num2;
        break;
      default:
        break;
    }

    currentInput = result.toString();
    displayElement.value = currentInput;
    currentOperation = '';
  }

  function calculateSquareRoot() {
    const num = parseFloat(currentInput);
    const result = Math.sqrt(num);
    currentInput = result.toString();
    displayElement.value = currentInput;
  }

  function clearDisplay() {
    currentInput = '';
    storedValue = '';
    currentOperation = '';
    displayElement.value = '';
  }