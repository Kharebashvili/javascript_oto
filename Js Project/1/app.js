function styleText(text) {
    const styledText = `
      <span style="font-style: italic; text-decoration: underline; font-weight: bold;">
        ${text}
      </span>
    `;
  
    document.getElementById('task').innerHTML = `<h1>${styledText}</h1>`;
  }
  
  const Text_task = "Mogesalmebit!";
  styleText(Text_task);


  function setTextWithFontSize(text, fontSize) {
    const styledText = `
      <span style="font-size: ${fontSize}px;">
        ${text}
      </span>
    `;
  
    document.getElementById('task2').innerHTML = styledText;
  }
  
  const Text_task2 = "Textis Fontis Gazrda";
  const customFontSize = 50; // ჩავწერთ სასურველ ფონტის ზომას 
  setTextWithFontSize(Text_task2, customFontSize);
  
  

  