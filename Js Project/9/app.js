document.addEventListener("DOMContentLoaded", function () {
    const movingBox = document.getElementById("movingBox");

    function move(direction) {
        const currentX = parseFloat(getComputedStyle(movingBox).left);
        const currentY = parseFloat(getComputedStyle(movingBox).top);
        let newX = currentX;
        let newY = currentY;

        switch (direction) {
            case 'up':
                newY -= 50;
                break;
            case 'down':
                newY += 50;
                break;
            case 'left':
                newX -= 50;
                break;
            case 'right':
                newX += 50;
                break;
            default:
                break;
        }

        movingBox.style.transform = `translate(${newX}px, ${newY}px)`;
    }
});

// დაწერეთ ფუნქცია, რომელიც პარამეტრად გადაცემულ ობიექტს შვილობილად დაამატებს ასევე პარამეტრად გადაცემულ რაიმე HTML ობიექტს.


function addChildToParent(parentElement, childElement) {
    parentElement.appendChild(childElement);
  }
  

  const parentDiv = document.getElementById('parent'); 
  const childElement = document.createElement('div'); 
  

  childElement.textContent = 'This is the child element';
  addChildToParent(parentDiv, childElement);
  