// 2.	დაწერეთ ფუნქცია, რომელიც დაითვლის სტრიქონში სიმბოლო a-ის რაოდენობას.

function countCharacterA(inputString) {
    const numberOfAs = (inputString.match(/a/g) || []).length;
    return numberOfAs;
  }
  
  const myString = "Gamarjoba"; // ვწერთ სიტყვას სადაც გვჭირდება a ასოს დათვლა
  const result = countCharacterA(myString);
  
  console.log(`Number of 'a' characters: ${result}`);

//   5.	დაწერეთ ფუნქცია, რომელიც დააგენერირებს 40 შემთხვევითი სიმბოლოსაგან  შემდგარ სიტყვას.

function generateRandomWord() {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';
    let randomWord = '';
  
    for (let i = 0; i < 40; i++) {
      const randomIndex = Math.floor(Math.random() * alphabet.length);
      randomWord += alphabet[randomIndex];
    }
  
    return randomWord;
  }
  
  const randomWord = generateRandomWord();
  console.log(randomWord);