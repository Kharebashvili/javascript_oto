function task1(n){
    document.write(Math.ceil(n));
}

task1(7.7)

function task3(n){
    document.write(Math.round(n));
}
task3(4.8)

// 2.	დავწეროთ ფუნქცია, რომელიც პარამეტრად გადაცემულ რიცხვს დაამრგვა¬ლებს ნაკლებობით.

function task2(n){
    document.write(Math.floor(n));
}
task2(3.1)

// 4.	დავწეროთ ფუნქცია, რომელიც პარამეტრად გადაცემულ რიცხვს დაამ¬რგვალებს, დაამრგვალებს მეტობით ან ნაკლებობით, მეორე გა-დაცე¬მული პა-რა¬მეტრის მიხედვით (მეორე პარამეტრი უნდა იყოს ლოგიკური).



function task4(n, t){
    switch(t){
        case 1: document.write("<h1>"+Math.ceil(n)+"</h1>")
        break
        case 2: document.write("<h1>"+Math.floor(n)+"</h1>")
        break
        case 3: document.write("<h1>"+Math.round(n)+"</h1>")
        break
        default: document.write("<h1>"+n+"</h1>")
    }
}
task4(9.5)