// დაწერეთ ფუნქცია,  რომელიც გამოიტანს შემთხვევითი n რაოდენობის კენტ რიცხვს [k, p] შუალედიდან, (n, k, p-ს შეტანა განახორციელეთ html-ფორმიდან, ააგეთ შესაბამისი გარემო).


function generateRandomNumbers() {
    const n = parseInt(document.getElementById('n').value, 10);
    const k = parseInt(document.getElementById('k').value, 10);
    const p = parseInt(document.getElementById('p').value, 10);

    if (isNaN(n) || isNaN(k) || isNaN(p)) {
      alert('გთხოვთ შეიყვანოთ სწორი მონაცემები.');
      return;
    }

    if (n <= 0 || k >= p) {
      alert('არასწორი შენატანი. დარწმუნდით რომ n არის დადებითი რიცხვი და k < p.');
      return;
    }

    const oddNumbers = [];

    while (oddNumbers.length < n) {
      const randomNum = Math.floor(Math.random() * (p - k + 1)) + k;
      if (randomNum % 2 !== 0) {
        oddNumbers.push(randomNum);
      }
    }

    const resultElement = document.getElementById('result');
    resultElement.textContent = `შემთხვევითი კენტი რიცხვები: ${oddNumbers.join(', ')}`;
  }


//   ააგეთ html ფორმა შემდეგი ველებით:  
// ✔	სახელი;
// ✔	გვარი;
// ✔	პირადობის ნომერი;
// ✔	რეგისტრაციის თარიღი;
// ✔	ელექტრონული ფოსტა;
// დაწერეთ ფუნქცია, რომელიც შეამოწმებს სახელის და გვარის ველი ცარიელია თუ არა.
// დაწერეთ ფუნქცია, რომელიც შეამოწმებს პირადობის ნომერი შედგება თუ არა მხოლოდ 11 ციფრისგან. 
// დაწერეთ ფუნქცია, რომელიც რეგისტრაციის ველში ავტომატურად ჩაწერს მიმდინარე თარიღს (რიცხვი:თვე:წელი). 
// დაწერეთ ფუნქცია, რომელიც შეამოწმებს ელექტრონული ფოსტის ველში ჩაწერილი მონაცემი შეიცავს თუ არა “@” სიმბოლოს. 
// ზემოთ აღწერილი ფუნქციები გამოიყენეთ ღილაკზე დაჭერის შემდეგ არავალიდური მონაცემის შეტანის შემთხვევაში შესაბამისი შეტყობინების გამოტანისთვის ველის გასწვრივ(სახელი, გვარი, პირადი ნომერი, ელექტრონული ფოსტა).  



function isFieldEmpty(fieldValue, fieldName) {
    return fieldValue.trim() === '';
  }

  function isIdNumberValid(idNumber) {
    return /^\d{11}$/.test(idNumber);
  }

  function isEmailValid(email) {
    return email.includes('@');
  }

  function getCurrentDate() {
    const now = new Date();
    const day = now.getDate();
    const month = now.getMonth() + 1;
    const year = now.getFullYear();
    return `${day}:${month}:${year}`;
  }

  function validateForm() {
    const name = document.getElementById('name').value;
    const lastName = document.getElementById('lastName').value;
    const idNumber = document.getElementById('idNumber').value;
    const registrationDate = getCurrentDate();
    const email = document.getElementById('email').value;

    const nameError = document.getElementById('nameError');
    const lastNameError = document.getElementById('lastNameError');
    const idNumberError = document.getElementById('idNumberError');
    const registrationDateError = document.getElementById('registrationDateError');
    const emailError = document.getElementById('emailError');

    nameError.textContent = '';
    lastNameError.textContent = '';
    idNumberError.textContent = '';
    registrationDateError.textContent = '';
    emailError.textContent = '';

    if (isFieldEmpty(name, 'Name')) {
      nameError.textContent = 'Name cannot be empty';
    }

    if (isFieldEmpty(lastName, 'Last Name')) {
      lastNameError.textContent = 'Last Name cannot be empty';
    }

    if (!isIdNumberValid(idNumber)) {
      idNumberError.textContent = 'ID Number must consist of 11 digits';
    }

    document.getElementById('registrationDate').value = registrationDate;

    if (!isEmailValid(email)) {
      emailError.textContent = 'Invalid email format';
    }
  }


  // ააგეთ წრეწირის მოძრაობის ანიმაცია შესაბამისი მართვის ღილაკებით (მინიმუმ 4 ღილაკი), მოიფიქრეთ ანიმაციის მოძრაობის მიმართულებები.


  let animationInterval;
  let angle = 0;
  let direction = 1; // 1 საათის ისრის მიმართულებით, -1 საწინააღმდეგოდ

  function startAnimation() {
    if (!animationInterval) {
      animationInterval = setInterval(animate, 16); 
    }
  }

  function stopAnimation() {
    clearInterval(animationInterval);
    animationInterval = null;
  }

  function changeDirection(newDirection) {
    direction = newDirection === 'clockwise' ? 1 : -1;
  }

  function animate() {
    const circle = document.getElementById('circle');
    const radius = 100;
    const centerX = 150;
    const centerY = 150;

    angle += direction * 0.02; // მობრუნების სიჩქარე

    const x = centerX + radius * Math.cos(angle);
    const y = centerY + radius * Math.sin(angle);

    circle.style.left = `${x}px`;
    circle.style.top = `${y}px`;
  }