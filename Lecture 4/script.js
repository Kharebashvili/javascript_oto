const person = {
    name:"Nika",
    'lastname':"Chargeishvili",
    "age":18,
    education:{
        University:'Georgian American University',
        GPA:4.2,
        subject:"Web Programming JS"
    },
    hobbies:["fishing","travelling","basketball","reading"],
    getFullName:function(){
        console.log(this.name, this.lastname)
    }
}

console.log(person)
console.log(person.lastname)
console.log(person['lastname'])
console.log(person.education.GPA)
console.log(person.hobbies[1])
person.getFullName()

person.name = "Nikusha"
console.log(person)