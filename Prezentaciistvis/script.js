// განვსაზღვროთ კლასი სახელწოდებით "Person"
class Person {
    constructor(name, age) {
      this.name = name;
      this.age = age;
    }
  
    // პიროვნების შესახებ ინფორმაციის ჩვენების მეთოდი
    displayInfo() {
      console.log(`Sakheli: ${this.name}, Asaki: ${this.age}`);
    }
  }
  
  // შევქმნათ პიროვნების კლასის მაგალითი
  const person1 = new Person("Oto", 19);
  
  // Access properties and call methods of the instance
  console.log(person1.name);
  console.log(person1.age); 
  person1.displayInfo();



// განსხვავებული მეთოდი

//  class Animal {
//     speak() {
//       console.log('Some sound');
//     }
//   }
  
//   class Cat extends Animal {
//     purr() {
//       console.log('Purr...');
//     }
//   }
  
//   const myCat = new Cat();
//   myCat.speak();
//   myCat.purr(); 
