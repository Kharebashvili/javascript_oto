var divs = document.querySelectorAll("div")
console.log(divs)
divs[0].classList.add("container")
divs[1].classList.add("menu")


function add_circle() {
    let circ = document.createElement("div")
    circ.classList.add("circle")
    circ.style.backgroundColor = rand_color()
    let rad = rand_radius()
    circ.style.width = rad
    circ.style.height = rad
    // divs[0].appendChild(circ)
    divs[0].insertBefore(circ, divs[0].firstChild);

}

function rand_color() {
    let r = Math.floor(Math.random()*256)
    let g = Math.floor(Math.random()*256)
    let b = Math.floor(Math.random()*256)
    return "rgb("+r+","+g+","+b+")"
}

function rand_radius() {
    return 2*(Math.floor(Math.random()*46)+5)+"px"
}